//3. insert one
db.hotel.insert({
	name: "single",
	accomodates: 2,
	price: 1000,
	description: "Fits small family",
	rooms_available: 10,
	isAvailable: false
	
})

//4. Insert Many
db.hotel.insertMany(
{
	name: "double",
	accomodates: 3,
	price: 2000,
	description: "Fits small family",
	rooms_available: 5,
	isAvailable: false
	
},
 {
 	name: "queen",
	accomodates: 4,
	price: 4000,
	description: "A bed room with a queen sized bed",
	rooms_available: 15,
	isAvailable: false
	
 }
)

//5. find room with name double
db.hotel.find({name: "double"})

// 6.updateOne method to update the queen room and set the available to 0
db.hotel.updateOne(
	{
		name: "queen"

	},
	{
		$set: {
			rooms_available: 0
		
		}
	}
)
//7. delete all rooms that have 0 available
db.hotel.deleteMany({rooms_available: 0})